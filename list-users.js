const admin = require('firebase-admin');
// eslint-disable-next-line import/no-extraneous-dependencies
const { getCredentialPathAsync } = require('firebase-tools/lib/defaultCredentials');

(async () => {
  const refreshTokenPath = await getCredentialPathAsync();
  // eslint-disable-next-line no-console
  console.log(`getCredentialPathAsync()\n\t=> ${refreshTokenPath}\n`);

  const app = admin.initializeApp({
    credential: admin.credential.refreshToken(refreshTokenPath),
    projectId: process.env.PROJECT_ID,
  });

  try {
    const listUsersResult = await app.auth().listUsers();

    listUsersResult.users.forEach((user) => {
      // eslint-disable-next-line no-console
      console.log(`${user.uid} ${user.email}`);
    });
  } finally {
    await app.delete();
  }
})();
