# Firebase Tools and Admin
firebase login で作成されたクレデンシャルを Firebase Admin で利用するサンプル。


## 利用する
```
$ git clone git@gitlab.com:masakura/firebase-tools-and-admin.git
$ cd firebase-tools-and-admin
firebase-tools-and-admin$ npm install
firebase-tools-and-admin$ PROJECT_ID=<firebase project id> npm run list-users
```


## 解説
`firebase login` で `~/.config/configstore/firebase-tools.json` が作成され、ここにアクセストークンなどが記録されているが、これを Firebase Admin で利用することはできない。(`client_secret` や `client_id` が不足している)

`gcloud auth application-default login` で `~/.config/gcloud/application_default_credentials.json` が作成され、ここにアクセストークンなどが記録されているが、これも Firebase Admin で利用することはできない。(スコープや権限が不足しているように見える。頑張ればなんとかなりそうだけど...)

[list-users.js](./list-users.js) を読めばわかるように、Firebase Tools の `getCredentialPathAsync()` を呼び出せば、クレデンシャルを作成した上でそのパスを返してくれる。これは Firebase Admin の Refresh Token として使える形式。

```javascript
const refreshTokenPath = await getCredentialPathAsync();

const app = admin.initializeApp({
  credential: admin.credential.refreshToken(refreshTokenPath),
  projectId: process.env.PROJECT_ID,
});
```

この方法は Undocumented だけど、Firebase Cloud Functions の Emulator で使われている方法なので、しょっちゅう変わったりするものではないと思う。
